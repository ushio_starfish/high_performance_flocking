#version 150
precision mediump float;

uniform mat4	ciModelViewProjection;
uniform mat3	ciNormalMatrix;

in vec4		ciPosition;
in vec3		ciNormal;
in vec4		ciColor;
in vec2		ciTexCoord0;
in vec3		vInstancePosition;
in vec3		vInstanceVelocity;

out vec4	Color;
out vec3	Normal;
out vec2	TexCoord;

const vec3 _Y_ = vec3(0.0, 1.0, 0.0);
const vec3 _X_ = vec3(1.0, 0.0, 0.0);
mat3 to_rotation(vec3 forword) {
	vec3 xAxis;
	if(0.999 < abs(forword.y)) {
		xAxis = cross(forword, _X_);
	} else {
		xAxis = cross(forword, _Y_);
	}
	xAxis = normalize(xAxis);
	vec3 yAxis = cross(xAxis, forword);
	vec3 zAxis = forword;
	return mat3(xAxis, yAxis, zAxis);
}

void main( void )
{
	vec3 forword = normalize(vInstanceVelocity);
	mat3 rotation = to_rotation(forword);
	vec3 localp = rotation * (2.0 * ciPosition.xyz) + vInstancePosition;
	gl_Position	= ciModelViewProjection * vec4(localp, 1.0);

  // gl_Position	= ciModelViewProjection * ( 10.0 * ciPosition + vec4( vInstancePosition, 0.0 ) );
	Color 		= ciColor;
	Normal		= ciNormalMatrix * ciNormal;
	TexCoord = ciTexCoord0;
}
