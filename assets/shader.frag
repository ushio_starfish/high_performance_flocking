#version 150

uniform sampler2D uTex0;

in vec4	Color;
in vec3	Normal;
in vec2	TexCoord;

out vec4 oColor;

void main( void )
{
	// vec4 tex = texture( uTex0, TexCoord.st );
	// vec3 normal = normalize( -Normal );
	// float diffuse = max( dot( normal, vec3( 0.0, 0.0, -1.0 ) ), 0.0 );
	// oColor = tex * Color * mix(diffuse, 1.0f, 0.5f);

	vec3 N = normalize(Normal);
  vec3 L = normalize(vec3(0.0, 1.0, 0.3));

  // harf lambert
  float lambert = max(dot(N, L), 0.0);
  float half_lambert = lambert * 0.5 + 0.5;

  vec4 tex = texture( uTex0, TexCoord.st );
	oColor = tex * Color * half_lambert;
}
