﻿#pragma once

#include <vector>
#include <array>
#include <limits>
#include <functional>
#include <glm/glm.hpp>

#include <tbb/tbb.h>
#include <atomic>
#include <mutex>

template <int DIM, typename REAL = float>
struct KDTreeVectorSelector {

};
template <typename REAL>
struct KDTreeVectorSelector<1, REAL> {
	typedef glm::tvec1<REAL, glm::precision::highp> VectorType;
};
template <typename REAL>
struct KDTreeVectorSelector<2, REAL> {
	typedef glm::tvec2<REAL, glm::precision::highp> VectorType;
};
template <typename REAL>
struct KDTreeVectorSelector<3, REAL> {
	typedef glm::tvec3<REAL, glm::precision::highp> VectorType;
};
template <typename REAL>
struct KDTreeVectorSelector<4, REAL> {
	typedef glm::tvec4<REAL, glm::precision::highp> VectorType;
};

struct KDTreeRandom {
	uint32_t operator()() {
		unsigned t;
		t = x ^ (x << 11);
		x = y; y = z; z = w;
		return w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
	}
	uint32_t x = 123456789u;
	uint32_t y = 362436069u;
	uint32_t z = 521288629u;
	uint32_t w = 248834323u;
};

template <int DIM, typename REAL = float>
struct KDTree {
	typedef typename KDTreeVectorSelector<DIM, REAL>::VectorType VectorType;
	enum {
		Dimension = DIM
	};

	struct AABB {
		VectorType min_position = VectorType(-std::numeric_limits<float>::max());
		VectorType max_position = VectorType(std::numeric_limits<float>::max());

		std::tuple<AABB, AABB> divide(float border, int dimension) const {
			AABB L = *this;
			AABB R = *this;
			L.max_position[dimension] = border;
			R.min_position[dimension] = border;
			return std::make_tuple(L, R);
		}
	};

	struct Node {
		float border = 0.0f;
		AABB aabb;
		int indices_beg = 0;
		int indices_end = 0;

		bool has_value() const {
			return indices_end - indices_beg > 0;
		}
	};

	// 二分木操作
	// 添え字は1から
	inline int left_child(int index) {
		return index << 1;
	}
	inline int right_child(int index) {
		return (index << 1) + 1;
	}
	inline int parent(int index) {
		return index >> 1;
	}
	int depth_to_dimension(int depth) {
		return depth % DIM;
	}
	int sample_count(int all_count) {
		return std::min(all_count, 10);
	}
	void build(VectorType *value_ptr, int count, int depth_count, AABB aabb) {
		_value_ptr = value_ptr;

		// 深度分だけメモリを一括確保
		_indices.resize(count * depth_count);
		for (int i = 0; i < count; ++i) {
			_indices[i] = i;
		}

		// value_ptrの数だけ
		_node_hash.resize(count);

		_depth_count = depth_count;

		// Sum[2^k, {k, 0, n - 1}]
		int nodeCount = (2 << depth_count) - 1;
		_nodes.resize(nodeCount);

		std::atomic<int> indices_memory_head = count;
		this->build_recursive_boost(0, 1, 0, count, &indices_memory_head, aabb);
		// int indices_memory_head = count;
		// this->build_recursive(0, 1, 0, count, &indices_memory_head, aabb);
	}
	void build_recursive(int depth, int parent, int indices_beg, int indices_end, int *indices_memory_head, AABB aabb) {
		int dimension = this->depth_to_dimension(depth);
		int node_index = parent - 1;

		_nodes[node_index].indices_beg = indices_beg;
		_nodes[node_index].indices_end = indices_end;
		_nodes[node_index].aabb = aabb;

		if (depth + 1 < _depth_count) {
			// continue
		}
		else {
			return;
		}


		float border = 0.0f;
		int n = indices_end - indices_beg;
		assert(n > 0);
		int sample = this->sample_count(n);
		for (int i = 0; i < sample; ++i) {
			int sample_index = _random() % n;
			
			int value_index = _indices[indices_beg + sample_index];
			border += _value_ptr[value_index][dimension];
		}
		border /= sample;

		_nodes[node_index].border = border;

		// 必要なメモリ量を調べる
		int count_L = 0;
		int count_R = 0;
		for (int i = 0; i < n; ++i) {
			int value_index = _indices[indices_beg + i];
			float value = _value_ptr[value_index][dimension];
			if (value < border) {
				count_L++;
			}
			else {
				count_R++;
			}
		}
		// メモリの分配を行う
		int indices_beg_L = *indices_memory_head;
		int indices_end_L = *indices_memory_head + count_L;
		*indices_memory_head += count_L;

		int indices_beg_R = *indices_memory_head;
		int indices_end_R = *indices_memory_head + count_R;
		*indices_memory_head += count_R;

		int child_L = left_child(parent);
		int child_R = right_child(parent);

		// 実際に子供にインデックスの分配を行う
		int i_L = 0;
		int i_R = 0;
		for (int i = 0; i < n; ++i) {
			int value_index = _indices[indices_beg + i];
			float value = _value_ptr[value_index][dimension];
			if (value < border) {
				_indices[indices_beg_L + i_L] = value_index;
				_node_hash[value_index] = child_L - 1;
				i_L++;
			}
			else {
				_indices[indices_beg_R + i_R] = value_index;
				_node_hash[value_index] = child_R - 1;
				i_R++;
			}
		}

		AABB aabb_L;
		AABB aabb_R;
		std::tie(aabb_L, aabb_R) = aabb.divide(border, dimension);
		if (indices_end_L - indices_beg_L > 0) {
			this->build_recursive(depth + 1, left_child(parent), indices_beg_L, indices_end_L, indices_memory_head, aabb_L);
		}

		if (indices_end_R - indices_beg_R > 0) {
			this->build_recursive(depth + 1, right_child(parent), indices_beg_R, indices_end_R, indices_memory_head, aabb_R);
		}
	}

	void build_recursive_boost(int depth, int parent, int indices_beg, int indices_end, std::atomic<int> *indices_memory_head, AABB aabb) {
		int dimension = this->depth_to_dimension(depth);
		int node_index = parent - 1;

		_nodes[node_index].indices_beg = indices_beg;
		_nodes[node_index].indices_end = indices_end;
		_nodes[node_index].aabb = aabb;

		if (depth + 1 < _depth_count) {
			// continue
		}
		else {
			return;
		}


		float border = 0.0f;
		int n = indices_end - indices_beg;
		assert(n > 0);
		int sample = this->sample_count(n);
		for (int i = 0; i < sample; ++i) {
			int sample_index = _random() % n;

			int value_index = _indices[indices_beg + sample_index];
			border += _value_ptr[value_index][dimension];
		}
		border /= sample;

		_nodes[node_index].border = border;

		// 必要なメモリ量を調べる
		int count_L = 0;
		int count_R = 0;
		for (int i = 0; i < n; ++i) {
			int value_index = _indices[indices_beg + i];
			float value = _value_ptr[value_index][dimension];
			if (value < border) {
				count_L++;
			}
			else {
				count_R++;
			}
		}
		// メモリの分配を行う
		//_mutex.lock();
		//int indices_beg_L = *indices_memory_head;
		//int indices_end_L = *indices_memory_head + count_L;
		//*indices_memory_head += count_L;

		//int indices_beg_R = *indices_memory_head;
		//int indices_end_R = *indices_memory_head + count_R;
		//*indices_memory_head += count_R;
		//_mutex.unlock();
		int indices_beg_L;
		int indices_end_L;
		int indices_beg_R;
		int indices_end_R;
		int expected;
		int indices_memory_head_now;
		do {
			expected = indices_memory_head->load();
			indices_memory_head_now = expected;
			indices_beg_L = indices_memory_head_now;
			indices_end_L = indices_memory_head_now + count_L;
			indices_memory_head_now += count_L;

			indices_beg_R = indices_memory_head_now;
			indices_end_R = indices_memory_head_now + count_R;
			indices_memory_head_now += count_R;
		} while (!indices_memory_head->compare_exchange_weak(expected, indices_memory_head_now));

		int child_L = left_child(parent);
		int child_R = right_child(parent);

		// 実際に子供にインデックスの分配を行う
		int i_L = 0;
		int i_R = 0;
		for (int i = 0; i < n; ++i) {
			int value_index = _indices[indices_beg + i];
			float value = _value_ptr[value_index][dimension];
			if (value < border) {
				_indices[indices_beg_L + i_L] = value_index;
				_node_hash[value_index] = child_L - 1;
				i_L++;
			}
			else {
				_indices[indices_beg_R + i_R] = value_index;
				_node_hash[value_index] = child_R - 1;
				i_R++;
			}
		}

		
		AABB aabb_L;
		AABB aabb_R;
		std::tie(aabb_L, aabb_R) = aabb.divide(border, dimension);

		if (depth < _boostDepth) {
			tbb::parallel_for(tbb::blocked_range<int>(0, 2, 1), [=](const tbb::blocked_range< int >& range) {
				for (int i = range.begin(); i != range.end(); i++) {
					if (i == 0) {
						if (indices_end_L - indices_beg_L > 0) {
							this->build_recursive_boost(depth + 1, left_child(parent), indices_beg_L, indices_end_L, indices_memory_head, aabb_L);
						}
					}
					else {
						if (indices_end_R - indices_beg_R > 0) {
							this->build_recursive_boost(depth + 1, right_child(parent), indices_beg_R, indices_end_R, indices_memory_head, aabb_R);
						}
					}
				}
			});
		}
		else {
			if (indices_end_L - indices_beg_L > 0) {
				this->build_recursive_boost(depth + 1, left_child(parent), indices_beg_L, indices_end_L, indices_memory_head, aabb_L);
			}
			if (indices_end_R - indices_beg_R > 0) {
				this->build_recursive_boost(depth + 1, right_child(parent), indices_beg_R, indices_end_R, indices_memory_head, aabb_R);
			}
		}
	}

	template <class F>
	void traverse(F &visitor) {
		this->traverse_recursive(0, 1, visitor);
	}

	template <class F>
	void traverse_recursive(int depth, int parent, F &visitor) {
		if (depth < _depth_count) {
			// continue
		}
		else {
			return;
		}

		int node_index = parent - 1;
		if (_nodes[node_index].has_value()) {
			visitor(depth, _nodes[node_index]);
			traverse_recursive(depth + 1, left_child(parent), visitor);
			traverse_recursive(depth + 1, right_child(parent), visitor);
		}
	}

	// 所属しているNodeを求める O(1)
	// 結果にindexは含まない
	template <class F>
	int neighbor_indices_for_index(int index, F &visitor) const {
		int hash_index = _node_hash[index];
		auto node = _nodes[hash_index];
		for (int i = node.indices_beg; i < node.indices_end; ++i) {
			int value = _indices[i];
			if (value != index) {
				visitor(value);
			}
		}
		return node.indices_end - node.indices_beg - 1;
	}

	/*
	// ノードを探索する O(Log N)
	void query_leaf(const VectorType &point, std::vector<int> &output_indices) {
		this->query_leaf_recursive(0, 1, point, output_indices);
	}

	void query_leaf_recursive(int depth, int parent, const VectorType &point, std::vector<int> &output_indices) {
		int dimension = this->depth_to_dimension(depth);
		int node_index = parent - 1;

		assert(_nodes[node_index].has_value());

		auto done = [=, &output_indices]() {
			output_indices.clear();
			for (int i = _nodes[node_index].indices_beg; i < _nodes[node_index].indices_end; ++i) {
				output_indices.push_back(_indices[i]);
			}
		};

		if (depth + 1 >= _depth_count) {
			// 最後の層
			done();
			return;
		}

		int child;
		if (point[dimension] < _nodes[node_index].border) {
			child = left_child(parent);
		}
		else {
			child = right_child(parent);
		}

		if (_nodes[child - 1].has_value()) {
			// 子が値を持っている
			this->query_leaf_recursive(depth + 1, child, point, output_indices);
		}
		else {
			// 持っていないので終了
			done();
		}
	}
	*/

	VectorType *_value_ptr = nullptr;
	int _value_count = 0;
	std::vector<int> _indices;
	std::vector<int> _node_hash; // value index -> node index

	int _depth_count = 0;
	std::vector<Node> _nodes;

	KDTreeRandom _random;

	std::mutex _mutex;

	int _boostDepth = 8;
};

