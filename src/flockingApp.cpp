﻿#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "cinder/CameraUi.h"
#include "cinder/Rand.h"
#include "CinderImGui.h"
#include "Cinder/ObjLoader.h"
#include "cinder/Perlin.h"

#include <vector>
#include <thread>

#include <glm/gtx/string_cast.hpp>
#include "KDTree.hpp"

#ifdef _MSC_VER
#ifdef _DEBUG
#define TBB_LIB_EXT "_debug.lib"
#else
#define TBB_LIB_EXT ".lib"
#endif
#pragma comment(lib, "tbb" TBB_LIB_EXT)
#pragma comment(lib, "tbbmalloc" TBB_LIB_EXT)
#endif
#include <tbb/tbb.h>

using namespace ci;
using namespace ci::app;
using namespace std;

const float random_size = 1000.0f;
const float random_velocity = 1.0f;

class flockingApp : public App {
  public:
	void setup() override;
	void cleanup() override;
	void mouseDown( MouseEvent event ) override;
	void update() override;
	void draw() override;

	void initialize_objects();
	void step();
private:
	CameraPersp	_camera;
	CameraUi	_camui;

	gl::BatchRef _plane;
	gl::BatchRef _sphere_batch;
	gl::TextureRef _texture;

	gl::BatchRef _shark;
	gl::TextureRef _shark_texture;

	typedef KDTree<3> KDTreeType;
	KDTreeType _kdtree;

	std::vector<KDTreeType::VectorType> _positions;
	std::vector<KDTreeType::VectorType> _velocities;
	std::vector<KDTreeType::VectorType> _positions_output;
	std::vector<KDTreeType::VectorType> _velocities_output;

	gl::GlslProgRef _glsl;
	gl::VboRef _instance_p_vbo;
	gl::VboRef _instance_v_vbo;

	std::future<int> _step_async;

	// Params
	int _depth = 17;
	int _object_count = 150000;
	float _separation_coef = 3.0f;
	float _separation_radius = 50.0f;
	float _cohesion_coef = 0.3f;
	float _alignment_coef = 0.066f;

	float _velocity_basic = 10.0f;
	float _velocity_basic_coef = 0.05f;

	float _centerize = 0.00005f;

	int _core_depth = 8;

	glm::vec3 _shark_position;
	glm::vec3 _shark_position_previous;
};


void flockingApp::setup()
{
	ui::initialize();

	_camera.lookAt(vec3(0.0f, 3.0f, 1.0f) * 500.0f, vec3(0));
	_camera.setPerspective(40.0f, getWindowAspectRatio(), 5.0f, 6000.0f);
	_camui = CameraUi(&_camera, getWindow());

	auto colorShader = gl::getStockShader(gl::ShaderDef().color());
	_plane = gl::Batch::create(geom::WirePlane().size(vec2(1000.0f)).subdivisions(ivec2(100)), colorShader);

	_glsl = gl::GlslProg::create(loadAsset("shader.vert"), loadAsset("shader.frag"));

	_texture = gl::Texture::create(loadImage(loadAsset("trevallieDiffuse.png")), gl::Texture::Format().mipmap());

	_shark_texture = gl::Texture::create(loadImage(loadAsset("shark/sharkDiffuse.png")), gl::Texture::Format().mipmap());
	auto sharkShader = gl::getStockShader(gl::ShaderDef().texture().color());
	ObjLoader shark(loadFile(app::getAssetPath("shark/shark.obj")));
	_shark = gl::Batch::create(shark, sharkShader);

	this->initialize_objects();
}
void flockingApp::cleanup() {
	if (_step_async.valid()) {
		_step_async.get();
	}
}
void flockingApp::initialize_objects() {
	ObjLoader loader = ObjLoader(loadFile(getAssetPath("trevallie.obj")));
	gl::VboMeshRef mesh = gl::VboMesh::create(loader);
	// gl::VboMeshRef mesh = gl::VboMesh::create(geom::Sphere());
	// gl::VboMeshRef mesh = gl::VboMesh::create(geom::Cube());

	_positions.clear();
	_velocities.clear();

	Rand r;
	for (int i = 0; i < _object_count; ++i) {
		float x = r.nextFloat(-random_size, random_size);
		float y = r.nextFloat(-random_size, random_size);
		float z = r.nextFloat(-random_size, random_size);
		_positions.emplace_back(x, y, z);

		float vx = r.nextFloat(-random_velocity, random_velocity);
		float vy = r.nextFloat(-random_velocity, random_velocity);
		float vz = r.nextFloat(-random_velocity, random_velocity);
		_velocities.emplace_back(vx, vy, vz);
	}

	_positions_output = _positions;
	_velocities_output = _velocities;

	_instance_p_vbo = gl::Vbo::create(GL_ARRAY_BUFFER, _positions.size() * sizeof(KDTreeType::VectorType), _positions.data(), GL_DYNAMIC_DRAW);
	_instance_v_vbo = gl::Vbo::create(GL_ARRAY_BUFFER, _velocities.size() * sizeof(KDTreeType::VectorType), _velocities.data(), GL_DYNAMIC_DRAW);

	{
		geom::BufferLayout instanceDataLayout;
		instanceDataLayout.append(geom::Attrib::CUSTOM_0, 3, 0, 0, 1 /* per instance */);
		mesh->appendVbo(instanceDataLayout, _instance_p_vbo);
	}
	{
		geom::BufferLayout instanceDataLayout;
		instanceDataLayout.append(geom::Attrib::CUSTOM_1, 3, 0, 0, 1 /* per instance */);
		mesh->appendVbo(instanceDataLayout, _instance_v_vbo);
	}

	// and finally, build our batch, mapping our CUSTOM_0 attribute to the "vInstancePosition" GLSL vertex attribute
	// sphere_batch = gl::Batch::create(mesh, _glsl, { { geom::Attrib::CUSTOM_0, "vInstancePosition" } });
	_sphere_batch = gl::Batch::create(mesh, _glsl, { { geom::Attrib::CUSTOM_0, "vInstancePosition" }, { geom::Attrib::CUSTOM_1, "vInstanceVelocity" } });
}

void flockingApp::mouseDown( MouseEvent event )
{
}

void flockingApp::update()
{
	if (_step_async.valid()) {
		_step_async.get();
	}

	KDTreeType::VectorType *positions = (KDTreeType::VectorType *)_instance_p_vbo->mapReplace();
	memcpy(positions, _positions.data(), _positions.size() * sizeof(KDTreeType::VectorType));
	_instance_p_vbo->unmap();
	KDTreeType::VectorType *velocities = (KDTreeType::VectorType *)_instance_v_vbo->mapReplace();
	memcpy(velocities, _velocities.data(), _velocities.size() * sizeof(KDTreeType::VectorType));
	_instance_v_vbo->unmap();

	_kdtree._boostDepth = _core_depth;
	_step_async = std::async(std::launch::async, [=] {
		this->step();
		return 1;
	});

	double e = getElapsedSeconds();
	float ne = e * 0.05f;
	float s = 5000.0f;
	static Perlin perlin(0);
	glm::vec3 next_position;
	next_position.x = perlin.noise(ne, 0.0f) * s;
	next_position.y = perlin.noise(ne, 5.0f) * s;
	next_position.z = perlin.noise(ne, 10.0f) * s;

	//if (0.01f < glm::distance2(next_position, _shark_position)) {
	//	_shark_position_previous = _shark_position;
	//}
	_shark_position_previous = _shark_position;
	_shark_position = next_position;
}
void flockingApp::step() {
	KDTreeType::AABB aabb;
	aabb.min_position = KDTreeType::VectorType(-10000.0f);
	aabb.max_position = KDTreeType::VectorType(10000.0f);
	_kdtree.build(_positions.data(), _positions.size(), _depth, aabb);

	glm::vec3 shark = _shark_position;
	float separation_radius_squared = _separation_radius * _separation_radius;
	tbb::parallel_for(tbb::blocked_range<int>(0, _positions.size(), _positions.size() / 20), [=](const tbb::blocked_range< int >& range) {
		for (int i = range.begin(); i != range.end(); i++) {
			/*
			基本的に加速は固定量（生物が加速できる量は決まっているから）
			*/

			auto p = _positions[i];
			auto v = _velocities[i];

			KDTreeType::VectorType acceleration;

			KDTreeType::VectorType center;
			KDTreeType::VectorType velocity_average;
			KDTreeType::VectorType separation;

			int index_count = _kdtree.neighbor_indices_for_index(i, [=, &center, &velocity_average, &separation](int neighbor_index) {
				auto neighbor_p = _positions[neighbor_index];
				auto neighbor_v = _velocities[neighbor_index];

				center += neighbor_p;
				velocity_average += neighbor_v;

				// 分離
				KDTreeType::VectorType separation_dir = p - neighbor_p;
				float length_squared = glm::length2(separation_dir);
				if (0.001f < length_squared && length_squared < separation_radius_squared) {
					float length = glm::sqrt(length_squared);
					separation_dir /= length;
					separation += separation_dir;
				}
			});
			// 0チェックでゼロ割を回避
			if (index_count > 0) {
				float inverse_index_count = 1.0f / index_count;
				center *= inverse_index_count;
				velocity_average *= inverse_index_count;

				separation *= _separation_coef;
				separation *= inverse_index_count;

				// 分離を適用
				acceleration += separation;

				// 結合
				KDTreeType::VectorType cohesion_dir = center - p;
				float cohesion_length_squared = glm::length2(cohesion_dir);
				if (0.1f < cohesion_length_squared) {
					cohesion_dir /= glm::sqrt(cohesion_length_squared);
					acceleration += cohesion_dir * _cohesion_coef;
				}

				// 整列
				KDTreeType::VectorType alignment_dif = velocity_average - v;
				acceleration += alignment_dif * _alignment_coef;
			}

			// あんまり中心から離れないように
			KDTreeType::VectorType centerize = -p * _centerize;
			acceleration += centerize;

			// サメ
			constexpr float shark_radius = 700.0f;
			constexpr float shark_power = 600.0f;

			constexpr float shark_radius_squared = shark_radius * shark_radius;
			float shark_distance_squared = glm::distance2(shark, p);
			if (0.1f < shark_distance_squared && shark_distance_squared < shark_radius_squared) {
				auto shark_distance = glm::sqrt(shark_distance_squared);
				auto escape_dir = (p - shark) / shark_distance;
				auto escape = 1.0f - glm::smoothstep(shark_radius * 0.7f, shark_radius, shark_distance);
				acceleration += escape_dir * escape * shark_power;
			}

			v += acceleration;

			float velocity_length_squared = glm::length2(v);
			if (0.001f < velocity_length_squared) {
				KDTreeType::VectorType dir = v / glm::sqrt(velocity_length_squared);
				KDTreeType::VectorType dir_standard = dir * _velocity_basic;
				v = glm::mix(v, dir_standard, _velocity_basic_coef);
			}

			_velocities_output[i] = v;
			_positions_output[i] = _positions[i] + v;
		}
	});

	std::swap(_velocities, _velocities_output);
	std::swap(_positions, _positions_output);
}

glm::mat3 look_at_forword(glm::vec3 forword) {
	const glm::vec3 _Y_ = glm::vec3(0.0f, 1.0f, 0.0f);
	const glm::vec3 _X_ = glm::vec3(1.0f, 0.0f, 0.0f);

	glm::vec3 xAxis;
	if (0.999f < glm::abs(forword.y)) {
		xAxis = glm::cross(forword, _X_);
	}
	else {
		xAxis = glm::cross(forword, _Y_);
	}
	xAxis = glm::normalize(xAxis);
	glm::vec3 yAxis = glm::cross(xAxis, forword);
	glm::vec3 zAxis = forword;
	return glm::mat3(xAxis, yAxis, zAxis);
}

void flockingApp::draw()
{
	// Gray background.
	gl::clear(Color::gray(0.5f));

	// Set up the camera.
	gl::ScopedMatrices push;
	gl::setMatrices(_camera);

	// Enable depth buffer.
	gl::ScopedDepth depth(true);
	gl::ScopedBlend blend(false);
	gl::ScopedFaceCulling culling(true, GL_FRONT);

	// Draw the grid on the floor.
	{
		gl::ScopedColor color(Color::gray(0.2f));
		_plane->draw();
	}

	//for (int i = 0; i < _positions.size(); ++i) {
	//	gl::ScopedMatrices push;
	//	gl::translate(_positions[i]);
	//	_sphere->draw();
	//}

	_texture->bind();
	_sphere_batch->drawInstanced(_positions.size());
	_texture->unbind();
	// gl::drawSphere(glm::vec3(), 1.0f);

	{
		gl::ScopedModelMatrix m;

		gl::translate(_shark_position);
		float s = 10.0f;
		gl::scale(s, s, s);

		auto r = look_at_forword(glm::normalize(_shark_position - _shark_position_previous));
		gl::multModelMatrix(glm::mat4(r));
		
		gl::ScopedColor c(glm::vec4(0.5f));
		// gl::ScopedFaceCulling culling(true, GL_FRONT);
		_shark_texture->bind();
		_shark->draw();
		_shark_texture->unbind();
	}


	ui::ScopedWindow window("Flock Settings");

	ImGui::Text("fps: %.1f", getAverageFps());

	if (ImGui::SliderInt("object count", &_object_count, 1000, 300000)) {
		if (_step_async.valid()) {
			_step_async.get();
		}
		this->initialize_objects();
	}
	ImGui::SliderInt("depth", &_depth, 1, 20);
	// 

	ImGui::SliderFloat("separation", &_separation_coef, 0.0f, 100.0f);
	ImGui::SliderFloat("separation radius", &_separation_radius, 0.0f, 100.0f);
	ImGui::SliderFloat("cohesion", &_cohesion_coef, 0.0f, 10.0f);
	ImGui::SliderFloat("alignment", &_alignment_coef, 0.0f, 1.0f);

	ImGui::SliderFloat("velocity basic", &_velocity_basic, 0.0f, 50.0f);
	ImGui::SliderFloat("velocity basic coef", &_velocity_basic_coef, 0.0f, 1.0f);

	ImGui::SliderInt("core depth", &_core_depth, 1, 20);
}

auto options = RendererGl::Options();
// auto options = RendererGl::Options().version(3, 0);
//
CINDER_APP(flockingApp, RendererGl(options), [](App::Settings *settings) {
	settings->setWindowSize(1920, 1000);
});
